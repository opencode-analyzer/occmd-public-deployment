_PY_FILES=occmd ./src/*.py

_NAME = occmd
_COMMIT = $(shell git rev-list HEAD -n 1)
_TAG = $(_NAME)_$(_COMMIT)
_REG_IMG = registry.opencode.de/opencode-analyzer/occmd-public:$(_COMMIT)

.PHONY: default lint test docker-build docker-run

default: lint

lint:
	pylint $(_PY_FILES) || true
	mypy $(_PY_FILES) || true
	pyright $(_PY_FILES) || true
	semgrep scan $(_PY_FILES) --config p/bandit \
		--config p/python \
		--config p/gitlab \
		--config p/trailofbits \
		--config p/r2c-best-practices \
		--config p/secrets \
		--config p/r2c-bug-scan \
		--config p/insecure-transport \
		|| true
	bandit -r -b .bandit_baseline src occmd || true
	pyre || true


test:
	pytest tests

docker-build:
	docker build 							\
		-t $(_TAG) 						\
		--build-arg DUMMY=`date +%s`				\
		- < Dockerfile
	docker image tag $(_TAG) $(_REG_IMG)

docker-push:
	docker login 							\
		-u 'oc000014832448@anonym.gov' 				\
		-p ${OC_GL_APIKEY} registry.opencode.de
	docker push $(_REG_IMG)

docker-pull:
	docker pull $(_REG_IMG)
	docker image tag $(_REG_IMG) $(_TAG)

# If the user does not select a target project, run on the example project
OCCMD_ID ?= 1108

ifdef OCCMD_TARGET
	_TARGET_MOUNT = --mount "type=bind,src=$(OCCMD_TARGET),dst=/opt/target"
else
	_TARGET_MOUNT =
endif

ifndef OC_GL_APIKEY
	$(error OC_GL_APIKEY is not set)
endif

docker-run:
	docker run 							\
		--rm 							\
		-e OC_GL_APIKEY=$(OC_GL_APIKEY) 			\
		--mount "type=volume,src=res_$(_TAG),dst=/opt/occmd/resources" \
		$(_TARGET_MOUNT)					\
		$(_TAG)							\
		-i $(OCCMD_ID) $(OCCMD_ARGS)
