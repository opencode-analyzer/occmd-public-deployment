# occmd

The (o)pen(c)ode (c)o(m)man(d) for running checks on projects on OpenCoDE.

## Setup

### Environment Variables

The following environment variables must be set:

- `OC_GL_APIKEY`: GitLab API key

### Configuration (local installation)

Copy the example configuration file from `examples/occmdcfg.ini` into the project root. You have to change at least the `resources_dir` in the `DEFAULT` section. Persistent data structures will be created and searched in this location. You should probably set this to the `./resouces` directory, or at least copy its contents to the alternative location you chose.

### Docker

You can try out `occmd` via Docker.

#### Image
To build the container image yourself run
```
make docker-build
```
Alternatively: To fetch the latest image from the OpenCoDE container registry run
```
make docker-pull
```
Optionally: To check if you successfully downloaded the image run
```
make docker-run
```

#### Run
You need to set 2 parameters
1. `OCCMD_ID`: set this to the GitLab ID of the project on OpenCoDE
2. `OCCMD_TARGET`: set this to the local file system path of the project-to-check

Then run
```
make docker-run OCCMD_ID=<value> OCCMD_TARGET=<value>
```

This will run all available checks. You can select a single check via the `OCCMD_ARGS` parameter. Anything you put into this parameter will the appended to the `occmd` invocation.

### Dependencies (local installation)

#### Python

To install Python dependencies, first ensure that you have Python virtual environments installed. Then, execute

```
python3 -m venv .env
source .env/bin/activate
pip3 install -r ./requirements.txt
```

#### Other

We might run some external programs. Thus, if you experience execution errors check the error message and install any missing dependencies via your distribution's package manager.

## Getting Started

### Documentation

To view the docs navigate to `docs/build/html/` and launch an http server
```
python -m http.server 7331 --directory $(pwd) > /tmp/serve.log 2>&1 &
```
You can now view the documentation in your favorite web browser
```
lynx localhost:7331
```

### Local Dump

To create or update you local dump of OpenCoDE run
```
./occmd update
```

### Dashboard

To create or update a collections of stats about the platform run
```
./occmd dashboard
```
You can also run some meta analyses on the whole platform, check out `help` for more information.

### Checks

To run all checks on all repos execute
```
./occmd check
```
You can also apply a single check or check only a single repository. Consult `help` for more information.

