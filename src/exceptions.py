"""
Exceptions that might be raised by core or check code.
"""


class CoreGoesBoomException(Exception):
    """
    Raised if core code encounters an unrecoverable error (uncaught).
    """

    pass


"""
Exceptions that might be raised by check code and must be caught by
core code.
"""


class CheckGoesBoomException(Exception):
    """
    Raise this exception if your check exploded. (at runtime)
    """

    pass


class CheckConstructionException(Exception):
    """
    Raise this exception if your check's constructor encountered some fatal
    error.
    """

    pass


class CheckNotApplicableException(Exception):
    """
    Raise this exception if your check comes to the conclusion that it cannot
    produce a reasonable result for the griven project. (at runtime)
    """

    pass
