"""
Implementation of the OpenCoDE Usage check, which attempts to determine
whether a repository is actively developed on OpenCode or if it is just a
mirror.
"""

import logging
from contextlib import suppress
from pathlib import Path
import re
from typing import Any, NamedTuple, Iterable

import yaml
from Levenshtein import distance
from gitlab import GitlabAuthenticationError, GitlabListError

from src.interfaces import CheckInterface
from src.utils import file_list, get_publiccode_cfg

logger = logging.getLogger(__name__)

README_MIRROR_KEYWORDS = [
    "is a mirror",
    "ist ein mirror",
    "pull mirror from",
    "push mirror from",
]
GITHUB_SQUASH_REGEX = re.compile(r"\(#\d+\)$")
EDIT_DISTANCE_LIMIT = 1


class MirrorCheckResult(NamedTuple):
    name: str
    confidence: float
    value: float


class OpencodeUsage(CheckInterface):
    """
    Class which represents a check that runs a list of heuristics against a
    given project in order to determine whether it is a mirror and returns a
    'score'.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._files_in_repo_root = {
            f.name: f for f in file_list(self.repo, recursive=False)
        }

    def run(self, args_dict: dict[str, Any] | None = None) -> dict[str, Any]:
        result: dict[str, Any] = super().run(args_dict)
        heuristics: list[MirrorCheckResult] = self._compute_mirror_heuristics()

        return {
            "score": self._compute_score(heuristics),
            "results": self._format_results(heuristics),
        } | result

    @staticmethod
    def _compute_score(heuristics: list[MirrorCheckResult]) -> float:
        if sum(h.value * h.confidence for h in heuristics) >= 1.0:
            return 0.0
        return 1.0

    @staticmethod
    def _format_results(
        heuristics: list[MirrorCheckResult],
    ) -> dict[str, list[dict[str, float | str]]]:
        return {
            "opencode_usage": [
                {"name": h.name, "value": h.value, "confidence": h.confidence}
                for h in heuristics
            ]
        }

    def _compute_mirror_heuristics(self) -> list[MirrorCheckResult]:
        return [
            self._check_committers(),
            self._check_description(),
            self._check_for_github_dir(),
            self._check_merge_requests(),
            self._check_publiccode_cfg(),
            self._check_readme(),
        ]

    def _check_description(self) -> MirrorCheckResult:
        return MirrorCheckResult(
            name="description",
            confidence=1.0,
            value=1.0 if self._mirror_in_description() else 0.0,
        )

    def _mirror_in_description(self) -> bool:
        """
        Checks the Gitlab project description for mentions of this repository
        being a mirror
        """
        return any(
            f"{mirror_type} mirror from"
            in (self.proj.description or "").lower()
            for mirror_type in ["push", "pull"]
        )

    def _check_readme(self):
        return MirrorCheckResult(
            name="readme",
            confidence=0.4,
            value=1.0 if self._mirror_in_readme() else 0.0,
        )

    def _mirror_in_readme(self) -> bool:
        """
        This is generally prone to false negatives
        """
        readme = self._find_readme()
        if readme is None:
            return False

        contents = readme.read_text(errors="ignore").lower()
        return any(k in contents for k in README_MIRROR_KEYWORDS)

    def _check_publiccode_cfg(self):
        return MirrorCheckResult(
            name="publiccode.yml",
            confidence=1.0,
            value=1.0 if self._mirror_in_publiccode_cfg() else 0.0,
        )

    def _mirror_in_publiccode_cfg(self) -> bool:
        """
        Looks at the contents of the publiccode.y(a)ml file to check if the
        configured URL is an OpenCoDE URL. The file being missing, not
        being parsable or not containing a URL is also treated as a mirror.
        """
        publiccode_cfg = get_publiccode_cfg(self.repo)
        if publiccode_cfg is None:
            return True

        try:
            publiccode = yaml.safe_load(publiccode_cfg.read_text())
        except yaml.YAMLError as error:
            logger.info(f"Project has invalid publiccode.yml: {error}")
            return True

        return "opencode.de" not in publiccode.get("url", "")

    def _find_readme(self) -> Path | None:
        for file_name in self._files_in_repo_root:
            if "readme" in file_name.lower():
                return self._files_in_repo_root[file_name]
        logger.info(f"No readme found in {self.proj.get_id()}")
        return None

    def _check_merge_requests(self) -> MirrorCheckResult:
        return MirrorCheckResult(
            name="merge requests",
            confidence=1.0,
            value=1.0 if self._merge_requests_indicate_mirror() else 0.0,
        )

    def _merge_requests_indicate_mirror(self) -> bool:
        """
        If merge request commits exist but no merge requests, this repository
        is probably a mirror (because merge requests don't get mirrored
        unlike commits)
        """
        return (
            self._merge_request_commit_exist()
            and not self._merge_requests_exist()
        )

    def _merge_requests_exist(self) -> bool:
        try:
            for _ in self.proj.mergerequests.list(
                scope="all", state="all", iterator=True
            ):
                return True
            return False
        except (GitlabListError, GitlabAuthenticationError):
            return True

    def _merge_request_commit_exist(self) -> bool:
        """
        Check if there are commits with default merge commit message (implying
        that a merge request was merged)
        """
        try:
            for commit in self.repo.iter_commits():
                if _is_merge_commit_message(commit.message):
                    return True
                return False
        except ValueError:  # there are no commits
            return False

    def _check_for_github_dir(self) -> MirrorCheckResult:
        return MirrorCheckResult(
            name=".github folder",
            confidence=0.6,
            value=1.0 if self._github_folder_exists() else 0.0,
        )

    def _github_folder_exists(self) -> bool:
        """
        If a .github folder exists, this is probably a mirror
        """
        try:
            repo_root = Path(self.repo.working_tree_dir)
        except TypeError:
            return False
        return (repo_root / ".github").is_dir()

    def _repository_mirroring_is_enabled(self) -> bool:
        # Future Work: if we have the necessary access rights we could
        # determine the target repository of helper repositories as documented
        # in https://gitlab.opencode.de/CoDE_Admin/repository-mirroring-ci-cd-example
        gitlab_ci_cfg = self._files_in_repo_root.get(".gitlab-ci.yml")
        if gitlab_ci_cfg is None:
            return False
        return "repository-mirroring" in gitlab_ci_cfg.read_text(
            errors="replace"
        )

    def _check_committers(self) -> MirrorCheckResult:
        return MirrorCheckResult(
            name="committers",
            confidence=0.4,
            value=self._get_share_of_unknown_committers(),
        )

    def _get_share_of_unknown_committers(self) -> float:
        """
        Look through names of committers and commit authors of the latest (up
        to 100) commits and determine if they are members of the repository.
        If they aren't a member, the commit probably wasn't authored on
        OpenCoDE. This is prone to false negatives if the user did not
        configure the same name in OpenCoDE as in the git config.

        :return: The average share of unknown commit authors.
        """
        members = self._get_members()
        commit_authors = self._find_commit_authors()

        total = len(commit_authors)
        if total == 0:
            return 0.0  # there are no commits in this repo

        unknown_authors = sum(
            1 for a in commit_authors if not self._contains_similar(members, a)
        )
        return unknown_authors / total

    def _find_commit_authors(self) -> list[str]:
        commiters = []
        with suppress(ValueError):  # will occur if there are no commits
            for commit in self.repo.iter_commits(max_count=100):
                # we count both the author and the committer
                commiters.append(commit.author.name.lower())
                commiters.append(commit.committer.name.lower())
        return commiters

    @staticmethod
    def _contains_similar(haystack: Iterable[str], needle: str) -> bool:
        """
        Checks if `haystack` contains a string that is similar to `needle`
        using the Levenshtein distance as metric.
        """
        return any(distance(needle, m) <= EDIT_DISTANCE_LIMIT for m in haystack)

    def _get_members(self) -> set[str]:
        """
        Retrieve all names of (direct) project members and members of groups
        that are part of the project.
        """
        project_members = {
            m.name.lower() for m in self.proj.members.list(iterator=True)
        }
        group_members = {
            m.name.lower()
            for g in self.proj.groups.list(iterator=True)
            for m in self.api.groups.get(g.id).members.list(iterator=True)
        }
        return project_members.union(group_members)


def _is_merge_commit_message(message: str) -> bool:
    return (
        _is_github_merge_commit_message(message)
        or _is_github_squash_message(message)
        or _is_gitlab_merge_commit_message(message)
    )


def _is_github_merge_commit_message(message: str) -> bool:
    return message.startswith("Merge pull request ")


def _is_github_squash_message(message: str) -> bool:
    """
    squashed GitHub pull request commits end with a " (#<PR_ID>)" in their
    message (e.g. "... (#1337)") which links back to the pull request.
    """
    return bool(GITHUB_SQUASH_REGEX.search(message))


def _is_gitlab_merge_commit_message(message: str) -> bool:
    return "See merge request" in message
