from pathlib import Path
from typing import (
    Iterable,
)


class SecretInterface:
    """Represents a single secret that was found by some tool"""

    def __init__(self):
        pass

    def summarize(self) -> dict:
        raise NotImplementedError


class SecretsToolInterface:
    """Represents a tool that can be applied to a project in order to
    discover secrets"""

    def __init__(self):
        pass

    @classmethod
    @property
    def name(cls):
        return cls.__name__

    def check_file(self, f: Path) -> None:
        """Checks the file 'f' for secrets and adds any found secrets
        to the internal state of the tool."""
        raise NotImplementedError

    def check_files(self, files: Iterable[Path]) -> None:
        """Checks the 'files' for secrets and adds any found secrets
        to the internal state of the tool."""
        raise NotImplementedError

    def create_or_overwrite_baseline(self, project_id: int):
        """Creates a new, or overwites an existing, baseline using the
        tool's internal state"""
        raise NotImplementedError

    def update_baseline(self, project_id: int) -> None:
        """Uses the tool's internal state to update an existing or
        create a new baseline on disk. Essentially performs an
        intersection between the internal state and the baseline if
        there is one, else it just writes the internal state to disk.

        Important: This method might change the tool's internal state.
        """
        raise NotImplementedError

    def diff_vs_baseline(
        self, project_id: int
    ) -> Iterable[SecretInterface]:
        """The list of all secrets the tool found in a project that are
        not in the baseline."""
        raise NotImplementedError

    def delete_baseline(self, project_id: int) -> None:
        """Removes any peristed baselines this tool has for the given
        project. No-op if there is no baseline."""
        raise NotImplementedError

    @property
    def detected_secrets(self) -> Iterable[SecretInterface]:
        """The list of all secrets the tool found in a project.
        Essentially the tools internal state."""
        raise NotImplementedError
