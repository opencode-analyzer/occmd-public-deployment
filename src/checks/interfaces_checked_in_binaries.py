from __future__ import annotations
from typing import Hashable, Tuple
from pathlib import Path


class FileTypeInterface:
    """Represents a recognized file type"""

    def __init__(self):
        pass

    def _key(self) -> Tuple[Hashable, ...]:
        """Used to decide equality in comparisons and hash based
        lookups"""
        raise NotImplementedError

    def __repr__(self) -> str:
        return str(self._key())

    def __str__(self) -> str:
        return str(self._key())

    def __hash__(self) -> int:
        return hash(self._key())

    def __eq__(self, other) -> bool:
        if isinstance(other, FileTypeInterface):
            return self._key() == other._key()
        raise NotImplementedError


class FileTypeToolInterface:
    """Represents a tool that implements the mapping
    file -> file_type"""

    def __init__(self):
        pass

    @classmethod
    @property
    def name(cls) -> str:
        return cls.__name__

    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

    def file_type_of(self, file: Path) -> FileTypeInterface:
        raise NotImplementedError
