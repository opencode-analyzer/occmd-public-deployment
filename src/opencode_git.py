"""Module to interact with OpenCoDE platform via git command line
tool"""

import os
import subprocess as sp
from pathlib import Path

from typing import Optional


class OpenCodeGit:
    """Class to interact with OpenCoDE platform via git command line
    tool"""

    username: str = os.environ.get("OC_GL_USER", default="foo")
    apikey: Optional[str] = os.environ.get("OC_GL_APIKEY", default=None)

    @classmethod
    def clone_project(cls, http_url: str, local_path: Path) -> None:
        # insert authentication
        http_url = (
                http_url[: http_url.find("gitlab")]
                + str(cls.username)
                + ":"
                + str(cls.apikey)
                + "@"
                + http_url[http_url.find("gitlab"):]
        )
        # do the clone
        sp.run(
            ["git", "clone", http_url, local_path.as_posix()],
            check=True,
            env={"GIT_TERMINAL_PROMPT": "0"},
        )
