### Adding a new Check

Follow these steps to add a new check.

1. Create a file `src/<name_of_your_check_in_snake_case>.py`
2. Define the check-specific output format of your check in a JSON schema. Place the schema in the resource directory under `schemas/check_<NameOfYourCheckInUpperCamelCase>_output_format.json`.
    - The `results` key of your check's output will be validated against this schema.
3. Implement a `class <NameOfYourCheckInUpperCamelCase>(CheckInterface)` in the file created at step 1
    - The parent constructor will populate two instance attributes `repo` and `proj` for you. The former is a `git.Repo` object representing a local copy of the repository to check. The latter is a `RESTObject` representing the remote repository on the OpenCoDE GitLab.
    - If your check runs many different external tools, you might want to abstract the tools and their types behind common interfaces. Add those in a separate `src/<name_of_your_check_in_snake_case>.py` file.
    - The check MUST NOT change the state of the local or remote repository in any way.
    - Your check's `run` method MUST return the result of your parent's `run` method as well as a `score` key that hold a float between 0 and 1 inclusive. It MAY include a `results` key with check-specific results that match the schema created at step 2
    - If your check blows up in a way that you cannot recover from, raise a `CheckException`.
    - If you detect that you cannot perform you check on a repository (in a way that leads to a remotely meaningful result), raise a `CheckNotApplicableException`
    - Your check can store resources in the resource directory under `checks/<name_of_your_check_in_snake_case>/`. There are two types of resources. First, resources that are checked into the remote occmd repository. Those will be available on each run, in particular, those are the only resources available at the beginning of the first run. They must be read-only at runtime. They might be managed as git subprojects to facilitate easier updating of existing installations, e.g., by (optionally) pulling at the beginning of a run. Second, generated resources that are not checked in to the remote repository. Those are created and managed by the application. All resources are preserved between runs.
4. Optional: Create your checks resource directory and populate it with checked-in resources.
    - You might want to use git submodules for easier updating.
    - The resources must be read-only at runtime.
5. Import your check class (the one that implements the `CheckInterface`) at the top if `src/checks.py` and add it to the `availableChecks` class variable of the `Checks` class in the same file.

That's it, congratulations, your check is now included in the occmd tool! It should show up in the output of `occmd check --help` under the possible values of the `-c` flag. The return value of your check will be written to stdout as a JSON object.
