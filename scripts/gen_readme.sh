#!/usr/bin/env bash

set -euo pipefail

cat docs/{intro,setup,getting_started}.md | tee README.md
