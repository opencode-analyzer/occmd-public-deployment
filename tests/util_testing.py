from typing import Type

from gitlab.v4.objects import Project
from git.repo import Repo

from src.opencode import OpenCode
from src.interfaces import CheckInterface


def get_check_instance_by_id(
    _id: int, check_class: Type[CheckInterface]
) -> CheckInterface:
    oc: OpenCode = OpenCode()
    proj: Project = oc.get_project_by_id(_id)
    repo: Repo = oc.get_repo_for_proj(proj)
    check: CheckInterface = check_class(proj, repo, api=oc.gl)

    return check
