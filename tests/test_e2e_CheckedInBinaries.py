from typing import Dict, Any

from src.checks.checked_in_binaries import CheckedInBinaries
from src.interfaces import CheckInterface

from tests.util_testing import get_check_instance_by_id


class TestE2eCheckedInBinaries:
    CHECKED_IN_BINARIES_BAD_01 = 1048
    CHECKED_IN_BINARIES_GOOD_01 = 1049

    def test_bad_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            self.CHECKED_IN_BINARIES_BAD_01, CheckedInBinaries
        )

        result: Dict[str, Any] = check.run(args_dict=None)

        assert result.get("score") == 0.0

    def test_good_01(self):
        check: CheckInterface = get_check_instance_by_id(
            self.CHECKED_IN_BINARIES_GOOD_01, CheckedInBinaries
        )

        result: Dict[str, Any] = check.run(args_dict=None)

        assert result.get("score") == 1.0
