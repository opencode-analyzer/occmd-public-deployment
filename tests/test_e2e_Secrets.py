from typing import Dict, Any

from src.checks.secrets import Secrets
from src.interfaces import CheckInterface

from tests.util_testing import get_check_instance_by_id


class TestE2eSecrets:
    SECRETS_BAD_01: int = 1187
    SECRETS_GOOD_01: int = 1188

    def test_bad_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            self.SECRETS_BAD_01, Secrets
        )

        result: Dict[str, Any] = check.run(args_dict={"baseline": 1})

        assert result.get("score", 1.0) < 0.05

    def test_good_01(self) -> None:
        check: CheckInterface = get_check_instance_by_id(
            self.SECRETS_GOOD_01, Secrets
        )

        result: Dict[str, Any] = check.run(args_dict={"baseline": 1})

        assert result.get("score") == 1.0
