import math
from typing import Dict, Any

from src.checks.opencode_usage import OpencodeUsage
from src.interfaces import CheckInterface

from tests.util_testing import get_check_instance_by_id

TEST_REPO_ID: int = 1871


def test_opencode_usage() -> None:
    check: CheckInterface = get_check_instance_by_id(
        TEST_REPO_ID, OpencodeUsage
    )

    result: Dict[str, Any] = check.run()

    assert result.get("score") == 0.0
    assert "opencode_usage" in result["results"]

    heuristics = {
        h["name"]: h["value"] for h in result["results"]["opencode_usage"]
    }
    assert heuristics["readme"] == 1.0
    assert heuristics["description"] == 0.0
    assert heuristics["merge requests"] == 1.0
    assert heuristics["publiccode.yml"] == 1.0
    assert heuristics[".github folder"] == 1.0
    assert 0 < heuristics["committers"] < 1
