ARG PYTHON_VERSION=3.11
FROM python:${PYTHON_VERSION}

# Install system requirements
# @file: file type plugin
# @gcc: a life without a compiler isn't worth living
# @wget: download stuff from the big evil internet
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        jq \
        file \
        gcc \
        git \
        wget \
    && rm -rf /var/cache/apt/archives /var/lib/apt/lists

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    DEBIAN_FRONTEND=noninteractive

# Install tokei for counting lines of code
WORKDIR /bin
RUN wget -O - https://github.com/XAMPPRocky/tokei/releases/download/v13.0.0-alpha.0/tokei-x86_64-unknown-linux-musl.tar.gz | tar zxf - \
    && which tokei

# Creates a non-root user with an explicit UID and adds permission to access
# the relevant directories.
RUN mkdir -p /opt/target /opt/occmd \
    && adduser -u 5678 --disabled-password --gecos "" ocuser \
    && chown -R ocuser /opt/target /opt/occmd

USER ocuser
WORKDIR /opt/occmd

# Install Python dependencies
COPY requirements.txt .
RUN python -m pip install --no-cache-dir -r requirements.txt

# Copy application files
COPY . .

# Prepare config file
RUN cp examples/occmdcfg.ini . \
    && sed -i 's/resources_dir =.*/resources_dir = \/opt\/occmd\/resources/' occmdcfg.ini

# Persist resources
VOLUME /opt/occmd/resources

# Limit container to check subcommand, expect project-to-check to be mounted at
# /opt/target. User can select check and must supply project ID.
ENTRYPOINT ["/opt/occmd/occmd", "check", "-d", "/opt/target"]
